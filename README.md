# homo.js

一个将任意数字解构成 114514 组合公式的工具。

[![vercel](https://vercelbadge.soraharu.com/?app=homo-js)](https://homo.soraharu.com/)

## 🌎 项目首页

[https://homo.soraharu.com/](https://homo.soraharu.com/)

## 📦️ 调用说明

```HTML
<script src="./homo.js"></script>
<script>
let 变恶臭 = homo(1919810);
// "114514*(11-4-5+14)+(114*514+(114*51*4+(1145*(1+4)+(11-4+5+1-4))))"
</script>
```

## 📜 开源许可

基于 [WTFPL](https://choosealicense.com/licenses/wtfpl/) 许可进行开源。
